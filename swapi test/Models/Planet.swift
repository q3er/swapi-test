//
//  Planet.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 14.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Planet: Object,Mappable {
    @objc dynamic var name:String?
    @objc dynamic var diameter:String?
    @objc dynamic var rotationPeriod:String?
    @objc dynamic var orbitalPeriod:String?
    @objc dynamic var gravity:String?
    @objc dynamic var population:String?
    @objc dynamic var climate:String?
    @objc dynamic var terrain:String?
    @objc dynamic var surfaceWater:String?
    var residents=List<String>()
    var films=List<String>()
    @objc dynamic var url:String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "url"
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        diameter <- map["diameter"]
        rotationPeriod <- map["rotation_period"]
        orbitalPeriod <- map["orbital_period"]
        gravity <- map["gravity"]
        population <- map["population"]
        climate <- map["climate"]
        terrain <- map["terrain"]
        surfaceWater <- map["surface_water"]
        residents <- map["residents"]
        films <- map["films"]
        url <- map["url"]
    }
    

}
