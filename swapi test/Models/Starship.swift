//
//  Starship.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 14.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Starship: Object,Mappable {
    @objc dynamic var name:String?
    @objc dynamic var model:String?
    @objc dynamic var starshipClass:String?
    @objc dynamic var manufacturer:String?
    @objc dynamic var costInCredits:String?
    @objc dynamic var length:String?
    @objc dynamic var crew:String?
    @objc dynamic var passengers:String?
    @objc dynamic var maxAtmospheringSpeed:String?
    @objc dynamic var hyperdriveRating:String?
    @objc dynamic var MGLT:String?
    @objc dynamic var cargoCapacity:String?
    @objc dynamic var consumables:String?
    var films=List<String>()
    var pilots=List<String>()
    @objc dynamic var url:String?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "url"
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        model <- map["model"]
        starshipClass <- map["starship_class"]
        manufacturer <- map["manufacturer"]
        costInCredits <- map["cost_in_credits"]
        length <- map["length"]
        crew <- map["crew"]
        passengers <- map["passengers"]
        maxAtmospheringSpeed <- map["max_atmosphering_speed"]
        hyperdriveRating <- map["hyperdrive_rating"]
        MGLT <- map["MGLT"]
        cargoCapacity <- map["cargo_capacity"]
        consumables <- map["consumables"]
        films <- map["films"]
        pilots <- map["pilots"]
        url <- map["url"]
    }
    

}
