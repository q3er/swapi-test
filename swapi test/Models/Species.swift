//
//  Spicies.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 14.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Species: Object,Mappable {
    @objc dynamic var name:String?
    @objc dynamic var classification:String?
    @objc dynamic var designation:String?
    @objc dynamic var averageHeight:String?
    @objc dynamic var averageLifespan:String?
    @objc dynamic var eyeColors:String?
    @objc dynamic var hairColors:String?
    @objc dynamic var skinColors:String?
    @objc dynamic var language:String?
    @objc dynamic var homeworld:String?
    var people=List<String>()
    var films=List<String>()
    @objc dynamic var url:String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "url"
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        classification <- map["classification"]
        designation <- map["designation"]
        averageHeight <- map["average_height"]
        averageLifespan <- map["average_lifespan"]
        eyeColors <- map["eye_colors"]
        hairColors <- map["hair_colors"]
        skinColors <- map["skin_colors"]
        language <- map["language"]
        homeworld <- map["homeworld"]
        people <- map["people"]
        films <- map["films"]
        url <- map["url"]
    }
    

}
