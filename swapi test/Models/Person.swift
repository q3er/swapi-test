//
//  Person.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 14.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//
import ObjectMapper
import ObjectMapperAdditions
import RealmSwift

class Person: Object,Mappable {
    @objc dynamic var name:String?
    @objc dynamic var height:String?
    @objc dynamic var mass:String?
    @objc dynamic var hairColor:String?
    @objc dynamic var skinColor:String?
    @objc dynamic var eyeColor:String?
    @objc dynamic var birthYear:String?
    @objc dynamic var gender:String?
    @objc dynamic var homeWorld:String?
    var films=List<String>()
    var species=List<String>()
    var vehicles=List<String>()
    var starships=List<String>()
    @objc dynamic var url:String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    

    override static func primaryKey() -> String? {
        return "url"
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        height <- map["height"]
        mass <- map["mass"]
        hairColor <- map["hair_color"]
        skinColor <- map["skin_color"]
        eyeColor <- map["eye_color"]
        birthYear <- map["birth_year"]
        gender <- map["gender"]
        homeWorld <- map["homeworld"]
        films <- (map["films"], RealmTypeCastTransform())
        species <- (map["species"], RealmTypeCastTransform())
        vehicles <- (map["vehicles"], RealmTypeCastTransform())
        starships <- (map["starships"], RealmTypeCastTransform())
        url <- map["url"]
    }
    

}
