//
//  Film.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 14.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Film: Object,Mappable {
    @objc dynamic var title:String?
    @objc dynamic var episodeId = 0
    @objc dynamic var openingCrawl:String?
    @objc dynamic var director:String?
    @objc dynamic var producer:String?
    @objc dynamic var releaseDate:String?
    var species=List<String>()
    var starships=List<String>()
    var vehicles=List<String>()
    var characters=List<String>()
    var planets=List<String>()
    @objc dynamic var url:String?

    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "url"
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        episodeId <- map["episode_id"]
        openingCrawl <- map["opening_crawl"]
        director <- map["director"]
        producer <- map["producer"]
        releaseDate <- map["release_date"]
        species <- map["species"]
        starships <- map["starships"]
        vehicles <- map["vehicles"]
        characters <- map["characters"]
        planets <- map["planets"]
        url <- map["url"]
    }
    
    
}
