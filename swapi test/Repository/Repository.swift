//
//  Repository.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 15.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol RepositoryProtocol {
    static var sharedInstance: RepositoryProtocol { get }
    func getPersons(_ query:String,completion:@escaping ([Person])->Void)
    func getPerson(_ id:String,completion:@escaping (Person)->Void)
    func getHomeworld(_ id:String,completion:@escaping (Planet)->Void)
    func getFilm(_ id:String,completion:@escaping (Film)->Void)
    func getSpecies(_ id:String,completion:@escaping (Species)->Void)
    func getVehicle(_ id:String,completion:@escaping (Vehicle)->Void)
    func getStarship(_ id:String,completion:@escaping (Starship)->Void)
}

class Repository: RepositoryProtocol {

    

    

    static let sharedInstance: RepositoryProtocol = Repository()
    
    let realmService:RealmServiceProtocol = RealmService.sharedInstance
    let serverService:ServerServiceProtocol = ServerService.sharedInstance
    
    private init(){
       
    }

    
    func getPersons(_ query:String,completion:@escaping ([Person])->Void){
        realmService.getPersons(query, completion: completion)
        serverService.sendSearchQuery(query, completion: {[unowned self] persons in
            for p in persons{
                self.realmService.setData(data: p)
            }
            completion(persons)
        })
        
    }

    func getPerson(_ id:String,completion:@escaping (Person)->Void){
        realmService.getData(ofType: Person.self,id:id, completion: completion, failure: {
            
        }())
        serverService.getItem(ofType: Person.self, id: id, completion: {[unowned self] person in
            self.realmService.setData(data: person)
            completion(person)
        }, failure: {
            
        }())
    }
    
    func getHomeworld(_ id: String, completion: @escaping (Planet) -> Void) {
        realmService.getData(ofType: Planet.self,id:id, completion: completion, failure: {
            
        }())
        serverService.getItem(ofType: Planet.self, id: id, completion: {[unowned self] planet in
            self.realmService.setData(data: planet)
            completion(planet)
        }, failure: {
            
        }())
    }
    
    func getFilm(_ id: String, completion: @escaping (Film) -> Void) {
        realmService.getData(ofType: Film.self,id:id, completion:completion, failure: {
            
        }())
        serverService.getItem(ofType: Film.self, id: id, completion: {[unowned self] film in
            self.realmService.setData(data: film)
            completion(film)
        }, failure: {
            
        }())
    }
    
    func getSpecies(_ id: String, completion: @escaping (Species) -> Void) {
        realmService.getData(ofType: Species.self,id:id, completion: completion, failure: {
            
        }())
        serverService.getItem(ofType: Species.self, id: id, completion: {[unowned self] species in
            self.realmService.setData(data: species)
            completion(species)
        }, failure: {
            
        }())
    }
    
    func getVehicle(_ id: String, completion: @escaping (Vehicle) -> Void) {
        realmService.getData(ofType: Vehicle.self,id:id, completion: completion, failure: {
            
        }())
        serverService.getItem(ofType: Vehicle.self, id: id, completion: {[unowned self] vehicle in
            self.realmService.setData(data: vehicle)
            completion(vehicle)
        }, failure: {
            
        }())
    }
    
    func getStarship(_ id: String, completion: @escaping (Starship) -> Void) {
        realmService.getData(ofType: Starship.self,id:id, completion: completion, failure: {
            
        }())
        serverService.getItem(ofType: Starship.self, id: id, completion: {[unowned self] starship in
            self.realmService.setData(data: starship)
            completion(starship)
        }, failure: {
            
        }())
    }
}
