//
//  ServerService.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 15.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper



protocol ServerServiceProtocol {
    static var sharedInstance: ServerServiceProtocol { get }
    
    func sendSearchQuery(_ query:String,completion:@escaping ([Person])->Void)
    
    func getItem<T:Mappable>(ofType: T.Type , id:String, completion:@escaping (T)->Void,failure:Void)

}

class ServerService: ServerServiceProtocol {
    
    
    private let domenURL = "https://swapi.co/api/"
     static let sharedInstance: ServerServiceProtocol = ServerService()
    
    private init(){
        //reachabilityService = try! DefaultReachabilityService()
    }
    
    func sendSearchQuery(_ query:String,completion:@escaping ([Person])->Void){
        let requestString="\(domenURL)people/?search=\(query)"
        sendGetRequest(url: requestString, completion: {json in
        var persons = [Person]()
            (json["results"] as! [[String:Any]]).forEach{result in
                persons.append(Person.init(JSON: result)!)
            }
        completion(persons)
        }, failure: {error in
            
        })
    }
    
    func getItem<T>(ofType: T.Type, id: String, completion: @escaping (T) -> Void, failure: Void) where T : Mappable {
        sendGetRequest(url: id, completion: {json in
        completion(T.init(JSON: json)!)
        }, failure: {error in
            
        })
    }
    
    private func sendGetRequest(url: String, completion: @escaping ([String:Any]) -> Void, failure:@escaping (Error) -> Void) {
        print(url)
        
        let request: URLRequest = URLRequest(url: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!, cachePolicy:.useProtocolCachePolicy, timeoutInterval: 60.0)
        
        AF.request(request).validate().responseJSON { response in
            print("\(request) \(response)")
            switch response.result {
            case .success(let data):
                if let json = data as? [String:Any]{
                    completion(json)
                }
                case .failure(let error):
                    failure(error)
            }
        }
    }

    
    
}

