//
//  RealmService.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 15.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//
import RealmSwift

protocol RealmServiceProtocol {
    static var sharedInstance: RealmServiceProtocol { get }
    func setData(data:Object)
    func getData<T:Object>(ofType: T.Type , id:String, completion:@escaping (T)->Void,failure:Void)
    func getPersons(_ query:String,completion:@escaping ([Person])->Void)

    
}

class RealmService: RealmServiceProtocol {

    static let sharedInstance: RealmServiceProtocol = RealmService()
    private let realm = try! Realm()
    private init(){
        
    }

    
    func setData(data:Object){
        try! realm.write {
            realm.add(data, update: .modified)
        }
    }
    func getData<T:Object>(ofType: T.Type , id:String, completion:@escaping (T)->Void,failure:Void){
        let result=realm.object(ofType: T.self, forPrimaryKey: id)
        if let _ = result{
            completion(result!)
        }else{
            failure
        }
       
    }
    
    func getPersons(_ query:String,completion:@escaping ([Person])->Void){
        let results = realm.objects(Person.self).filter("name CONTAINS '\(query)'")
        completion(Array(results))
    }
}
