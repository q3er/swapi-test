//
//  SearchViewController.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 14.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

import UIKit

protocol SearchViewProtocol: class {
    func setData(_ data:[Person])
}





class SearchViewController: UIViewController, SearchViewProtocol {
    
    
    var presenter: SearchPresenterProtocol!
    let configurator: SearchConfiguratorProtocol = SearchConfigurator()

    var results:[Person]?
 
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
    }
    
    func setData(_ data:[Person]){
        results=data
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="showPeopleDetail"{
            let destController=segue.destination as! PeopleDetailViewController
            let row=tableView.indexPathForSelectedRow?.row
            destController.peopleUrl=results?[row!].url ?? ""
        }
    }
}

extension SearchViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath) as! ResultTableViewCell
        cell.setData((results?[indexPath.row])!)
        return cell

    }
}

extension SearchViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.count>=3{
            presenter.queryStringChanged(searchText)
        }
    }
       
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
       
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
       
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text=""
        searchBar.resignFirstResponder()
    }
}
