//
//  SearchConfigurator.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 22.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol SearchConfiguratorProtocol {
    func configure(with viewController: SearchViewController)
}


class SearchConfigurator: SearchConfiguratorProtocol {

    func configure(with viewController: SearchViewController) {
        let presenter = SearchPresenter(view: viewController)
        let interactor = SearchInteractor(presenter: presenter)
        let router = SearchRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }


}
