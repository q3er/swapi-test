//
//  SearchInteractor.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 15.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol SearchInteractorProtocol {
    func getSearchResults(_ query:String)
}



class SearchInteractor: SearchInteractorProtocol {
    weak var presenter: SearchPresenterProtocol!
    
    let repository:RepositoryProtocol = Repository.sharedInstance
       
       required init(presenter: SearchPresenterProtocol) {
           self.presenter = presenter
           //NotificationCenter.default.addObserver(self, selector: #selector(allUpdate), name: NSNotification.Name(rawValue: "AllUpdate"), object: nil)
       }

    
    func getSearchResults(_ query:String){
        repository.getPersons(query, completion: {[unowned self] persons in
            self.presenter.resultsRecieved(persons)
        })
    }
}
