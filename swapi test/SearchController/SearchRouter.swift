//
//  SeacrhRouter.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 22.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol SearchRouterProtocol {
    
}


class SearchRouter: SearchRouterProtocol {

    init(viewController: SearchViewController) {
        self.viewController = viewController
    }
    
    weak var viewController: SearchViewController!


}
