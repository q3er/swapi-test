//
//  SearchPresenter.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 15.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol SearchPresenterProtocol: class {
    var router: SearchRouterProtocol! { set get }
   
    func queryStringChanged(_ query:String)
    func resultsRecieved(_ data:[Person])
}


class SearchPresenter: SearchPresenterProtocol{

    weak var view: SearchViewProtocol!
    var router: SearchRouterProtocol!
    var interactor: SearchInteractorProtocol!
       
       
    required init(view: SearchViewProtocol) {
        self.view = view
           
    }

    func queryStringChanged(_ query:String){
        interactor.getSearchResults(query)
    }
    
    func resultsRecieved(_ data:[Person]){
        view.setData(data)
    }
}
