//
//  PeopleDetailPresenter.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 15.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol PeopleDetailPresenterProtocol: class {
    var router: PeopleDetailRouterProtocol! { set get }
    func getPersonData(_ id:String)
    func personDataGot(_ person:Person)
    func homeworldGot(_ homeworld:Planet)
    func filmGot(_ film:Film)
    func speciesGot(_ species:Species)
    func vehicleGot(_ vehicle:Vehicle)
    func starshipGot(_ starship:Starship)
}

class PeopleDetailPresenter: PeopleDetailPresenterProtocol {

       weak var view: PeopleDetailViewProtocol!
       var router: PeopleDetailRouterProtocol!
       var interactor: PeopleDetailInteractorProtocol!
       
       
       required init(view: PeopleDetailViewProtocol) {
           self.view = view
           
       }
    
    func getPersonData(_ id:String){
        interactor.getPersonData(id)
    }
    
    func personDataGot(_ person:Person){
        view.setPersonData(person)
        interactor.getHomeworldData(person.homeWorld ?? "")
        for s in person.films {
            interactor.getFilmData(s)
        }
        for s in person.species {
            interactor.getSpeciesData(s)
        }
        for s in person.vehicles {
            interactor.getVehicleData(s)
        }
        for s in person.starships {
            interactor.getStarshipData(s)
        }
    }
    
    func homeworldGot(_ homeworld:Planet){
        view.setHomeworld(homeworld)
    }
    func filmGot(_ film:Film){
        view.addFilm(film)
    }
    func speciesGot(_ species:Species){
        view.addSpecies(species)
    }
    func vehicleGot(_ vehicle:Vehicle){
        view.addVehicle(vehicle)
    }
    func starshipGot(_ starship:Starship){
        view.addStarship(starship)
    }
}
