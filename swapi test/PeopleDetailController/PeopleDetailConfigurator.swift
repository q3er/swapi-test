//
//  PeopleConfigurator.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 22.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol PeopleDetailConfiguratorProtocol {
    func configure(with viewController: PeopleDetailViewController)
}

class PeopleDetailConfigurator: PeopleDetailConfiguratorProtocol {

    func configure(with viewController: PeopleDetailViewController) {
        let presenter = PeopleDetailPresenter(view: viewController)
        let interactor = PeopleDetailInteractor(presenter: presenter)
        let router = PeopleDetailRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }


}
