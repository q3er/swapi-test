//
//  PeopleDetailViewController.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 14.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

import UIKit


protocol PeopleDetailViewProtocol: class {
    func setPersonData(_ person:Person)
    func setHomeworld(_ planet:Planet)
    func addFilm(_ film:Film)
    func addSpecies(_ species:Species)
    func addVehicle(_ vehicle:Vehicle)
    func addStarship(_ starship:Starship)
}

class PeopleDetailViewController: UIViewController, PeopleDetailViewProtocol {
    var presenter: PeopleDetailPresenterProtocol!
    let configurator: PeopleDetailConfiguratorProtocol = PeopleDetailConfigurator()
    
    var peopleUrl=""
    
    var films=[Film]()
    var species=[Species]()
    var vehicles=[Vehicle]()
    var starships=[Starship]()
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var hairLabel: UILabel!
    @IBOutlet weak var skinLabel: UILabel!
    @IBOutlet weak var eyeLabel: UILabel!
    @IBOutlet weak var birthlabel: UILabel!
    @IBOutlet weak var genderlabel: UILabel!
    @IBOutlet weak var homeWorldLabel: UILabel!
    
    @IBOutlet weak var filmsStackView: UIStackView!
    @IBOutlet weak var speciesStackView: UIStackView!
    @IBOutlet weak var vehiclesStackView: UIStackView!
    @IBOutlet weak var starshipsStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.getPersonData(peopleUrl)
    }
    
    func setPersonData(_ person:Person) {
        title=person.name
        nameLabel.text=person.name
        heightLabel.text=person.height
        massLabel.text=person.mass
        hairLabel.text=person.hairColor
        skinLabel.text=person.skinColor
        eyeLabel.text=person.eyeColor
        birthlabel.text=person.birthYear
        genderlabel.text=person.gender
        
    }
    
    func setHomeworld(_ planet:Planet){
        homeWorldLabel.text=planet.name
    }
    
    func addFilm(_ film:Film){
        if !films.contains(film) {
            films.append(film)
            let view=StackViewSectionView()
            view.setText(film.title ?? "")
            filmsStackView.addArrangedSubview(view)
        }
    }
    
    func addSpecies(_ species:Species){
        if !self.species.contains(species) {
            self.species.append(species)
            let view=StackViewSectionView()
            view.setText(species.name ?? "")
            speciesStackView.addArrangedSubview(view)
        }
    }
    
    func addVehicle(_ vehicle:Vehicle){
        if !vehicles.contains(vehicle) {
            vehicles.append(vehicle)
            let view=StackViewSectionView()
            view.setText(vehicle.name ?? "")
            vehiclesStackView.addArrangedSubview(view)
        }
    }
    
    func addStarship(_ starship:Starship){
        if !starships.contains(starship) {
            starships.append(starship)
            let view=StackViewSectionView()
            view.setText(starship.name ?? "")
            starshipsStackView.addArrangedSubview(view)
        }
    }
}
