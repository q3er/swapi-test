//
//  PeopleDetailInteractor.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 15.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol PeopleDetailInteractorProtocol {
    func getPersonData(_ id:String)
    func getHomeworldData(_ id:String)
    func getFilmData(_ id:String)
    func getSpeciesData(_ id:String)
    func getVehicleData(_ id:String)
    func getStarshipData(_ id:String)
}


class PeopleDetailInteractor: PeopleDetailInteractorProtocol {
    weak var presenter: PeopleDetailPresenterProtocol!
       
    let repository:RepositoryProtocol = Repository.sharedInstance
       
       required init(presenter: PeopleDetailPresenterProtocol) {
           self.presenter = presenter

       }

    func getPersonData(_ id:String){
        repository.getPerson(id, completion: {[unowned self] person in
            self.presenter.personDataGot(person)
        })
    }
    
    func getHomeworldData(_ id:String){
        repository.getHomeworld(id, completion: {[unowned self] planet in
            self.presenter.homeworldGot(planet)
        })
        
    }
    func getFilmData(_ id:String){
        repository.getFilm(id, completion: {[unowned self] film in
            self.presenter.filmGot(film)
        })
    }
    func getSpeciesData(_ id:String){
        repository.getSpecies(id, completion: {[unowned self] species in
            self.presenter.speciesGot(species)
        })
    }
    func getVehicleData(_ id:String){
        repository.getVehicle(id, completion: {[unowned self] vehicle in
            self.presenter.vehicleGot(vehicle)
        })
    }
    func getStarshipData(_ id:String){
        repository.getStarship(id, completion: {[unowned self] starship in
            self.presenter.starshipGot(starship)
        })
    }
}
