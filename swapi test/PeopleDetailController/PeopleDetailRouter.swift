//
//  PeopleRouter.swift
//  swapi test
//
//  Created by Sergey Abadzhev on 22.12.2019.
//  Copyright © 2019 Sergey Abadzhev. All rights reserved.
//

protocol PeopleDetailRouterProtocol {
    
}

class PeopleDetailRouter: PeopleDetailRouterProtocol {

        init(viewController: PeopleDetailViewController) {
        self.viewController = viewController
    }
    
    weak var viewController: PeopleDetailViewController!


}
