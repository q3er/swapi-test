//
//  StackViewSectionView.swift
//  swapi test
//
//  Created by Sergey on 10.01.2020.
//  Copyright © 2020 Sergey Abadzhev. All rights reserved.
//

import UIKit

class StackViewSectionView: UIView {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet var contentView: UIView!
    
    override init(frame:CGRect){
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder:NSCoder){
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("StackViewSectionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame=bounds
    }
    
    func setText(_ string:String){
        textLabel.text=string
    }
}
